package com.retrocenter.gamelist.manager.action;

import java.io.File;

import com.retrocenter.datafile.gamelist.GLGame;
import com.retrocenter.datafile.gamelist.GameList;
import com.retrocenter.datafile.gamelist.GameListParser;

public class NormalizePath {
    public static void normalize(File gameListFile) throws Exception {
        GameList gameList = GameListParser.parse(gameListFile);
        normalizeGameList(gameList);
    }

    public static void normalizeGameList(GameList gameList) {
        for (GLGame game : gameList.getGames()) {
            normalizeGame(game);
        }
    }

    public static void normalizeGame(GLGame game) {
        if (game.getPath() != null) {
            normalizePath(game.getPath());
            normalizePath(game.getImage());
            normalizePath(game.getMarquee());
            normalizePath(game.getVideo());
        }
    }

    private static String normalizePath(String path) {
        if (path == null)
            return null;
        if (!path.contains("/"))
            return "./" + path;
        if (path.contains("/")) {
            return "." + path.substring(path.lastIndexOf("/"));
        }
        return path;
    }
}
