package com.retrocenter.gamelist.manager.util;

import java.io.File;
import java.nio.file.Paths;

import com.retrocenter.datafile.gamelist.GameList;
import com.retrocenter.datafile.gamelist.GameListParser;
import com.retrocenter.datafile.retrocenterdb.RetrocenterDB;
import com.retrocenter.datafile.retrocenterdb.RetrocenterGamelistUtil;
import com.retrocenter.datafile.retrocenterdb.RetrocenterParser;
import com.retrocenter.gamelist.manager.RetrocenterConfig;
import com.retrocenter.util.FileUtil;

public class GamelistDatabase {
    public static RetrocenterDB readRetrocenterDB(File dir) throws Exception {
        File fileDb = Paths.get(dir.getAbsolutePath(), RetrocenterConfig.RETROCENTER_DB_FILE_NAME).toFile();
        RetrocenterDB db = fileDb.exists() ? RetrocenterParser.parse(fileDb) : null;
        File fileGl = Paths.get(dir.getAbsolutePath(), RetrocenterConfig.GAMELIST_DB_CLEAR_FILE).toFile();
        GameList gl = fileGl.exists() ? GameListParser.parse(fileGl) : null;
        if (db != null && gl == null) {
            gl = RetrocenterGamelistUtil.retrocenterDBToGamelist(db);
            FileUtil.writeFile(fileGl, gl.toString().getBytes());
        } else if (db == null && gl != null) {
            db = RetrocenterGamelistUtil.gamelistToRetrocenterDB(gl);
            FileUtil.writeFile(fileDb, db.toString().getBytes());
        } else if (db == null && gl == null) {
            db = RetrocenterGamelistUtil.createRetrocenterDBFromFolder(dir);
            FileUtil.writeFile(fileDb, db.toString().getBytes());
            gl = RetrocenterGamelistUtil.retrocenterDBToGamelist(db);
            FileUtil.writeFile(fileGl, gl.toString().getBytes());
        }
        return db;
    }

    public static void saveRetrocenterDB(RetrocenterDB db, File dir) throws Exception {
        System.out.println("Salvando DB em " + dir);
        File fileDb = Paths.get(dir.getAbsolutePath(), RetrocenterConfig.RETROCENTER_DB_FILE_NAME).toFile();
        FileUtil.writeFile(fileDb, db.toString().getBytes());
    }
}
