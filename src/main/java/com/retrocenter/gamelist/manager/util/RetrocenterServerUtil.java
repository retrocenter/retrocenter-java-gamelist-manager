package com.retrocenter.gamelist.manager.util;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import org.json.JSONObject;

import com.retrocenter.datafile.retrocenterdb.RetrocenterGame;
import com.retrocenter.gamelist.manager.RetrocenterConfig;

public class RetrocenterServerUtil {
    private static final HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();

    public static String findGameID(RetrocenterGame game) throws Exception {
        String id = null;
        if (game.getSha1() != null) {
            id = findGameID("/admin/api/platforms/files/" + game.getSha1() + "/sha1");
        }
        if (id == null && game.getMd5() != null) {
            id = findGameID("/admin/api/platforms/files/" + game.getMd5() + "/md5");
        }
        if (id == null && game.getCrc() != null) {
            id = findGameID("/admin/api/platforms/files/" + game.getCrc() + "/crc");
        }
        return id;
    }

    private static String findGameID(String uri) throws Exception {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(RetrocenterConfig.getServerURL() + uri))
                .build();
        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        String body = response.body();
        JSONObject json = new JSONObject(body);
        if (json.keySet().contains("_embedded")) {
            long id = json.getJSONObject("_embedded")
                    .getJSONArray("adminPlatformArtifactFileDTOList")
                    .getJSONObject(0).getLong("id");
            return String.valueOf(id);
        }
        return null;
    }
}
