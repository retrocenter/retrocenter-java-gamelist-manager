package com.retrocenter.gamelist.manager.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.retrocenter.gamelist.manager.RetrocenterConfig;
import com.retrocenter.gamelist.manager.manager.PlatformItem;

public class PlatformUtil {
    public static List<PlatformItem> listPlatforms() {
        List<PlatformItem> result = new ArrayList<>();
        File[] dirs = new File(RetrocenterConfig.getROMsBaseDestDir()).listFiles();
        if (dirs != null) {
            for (File dir : dirs) {
                if (dir.isDirectory()) {
                    PlatformItem p = getFromName(dir.getName());
                    if (p != null) {
                        result.add(p);
                    }
                }
            }
        }
        return result;
    }

    public static PlatformItem getFromName(String name) {
        switch (name) {
            case "atari2600":
            case "Atari 2600":
                return new PlatformItem("atari2600", "Atari 2600", -1l);
            case "mastersystem":
                return new PlatformItem("mastersystem", "Sega Master System", -1l);
            case "megadrive":
                return new PlatformItem("megadrive", "Sega Mega Drive", -1L);
        }
        return null;
    }
}
