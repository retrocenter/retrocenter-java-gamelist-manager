package com.retrocenter.gamelist.manager.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class FileUtil {
    public static Map<Long, List<String>> listCRCOfFile(File rom) throws Exception {
        Map<Long, List<String>> files = new HashMap<>();
        if (rom.getName().toLowerCase().endsWith(".zip")) {
            try (ZipFile zipFile = new ZipFile(rom)) {
                Enumeration<? extends ZipEntry> e = zipFile.entries();
                while (e.hasMoreElements()) {
                    ZipEntry entry = e.nextElement();
                    String entryName = entry.getName();
                    Long crc = entry.getCrc();
                    List<String> l = files.get(crc);
                    if (l == null) {
                        l = new LinkedList<>();
                        files.put(crc, l);
                    }
                    l.add(rom.getName() + " [/" + entryName + "]");
                }
            } catch (Exception ex) {
                System.out.println("**** [ERRO] " + ex.getMessage() + ": " + rom);
            }
        } else {
            CRC32 crc32 = new CRC32();
            InputStream is = new BufferedInputStream(new FileInputStream(rom));
            int cnt;
            while ((cnt = is.read()) != -1) {
                crc32.update(cnt);
            }
            Long crc = crc32.getValue();
            List<String> l = files.get(crc);
            if (l == null) {
                l = new LinkedList<>();
                files.put(crc, l);
            }
            l.add(rom.getName());
            crc32.reset();
        }

        return files;
    }

    public static Map<Long, List<String>> listCRCROMs(File romDir) throws Exception {
        Map<Long, List<String>> files = new HashMap<>();
        File[] romFiles = romDir.listFiles();
        if (romFiles != null) {
            for (File rom : romFiles) {
                if (!rom.isFile())
                    continue;
                if (rom.getName().toLowerCase().startsWith("gamelist")
                        && rom.getName().toLowerCase().endsWith(".xml"))
                    continue;
                files.putAll(listCRCOfFile(rom));
            }
        }
        return files;
    }
}
