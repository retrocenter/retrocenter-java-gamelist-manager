package com.retrocenter.gamelist.manager.screenscraper;

import java.util.LinkedList;
import java.util.List;

import com.retrocenter.gamelist.manager.RetrocenterConfig;
import com.retrocenter.screenscraper.client.request.parser.RequestUserInfo;

public class ScreenScraperConfig {
    private static ScreenScraperConfig config = null;

    private RetrocenterConfig retrocenterConfig;

    public static ScreenScraperConfig getInstance() {
        if (config == null) {
            config = new ScreenScraperConfig();
        }

        return config;
    }

    public RequestUserInfo getLoginInfo() {
        return new RequestUserInfo(
                RetrocenterConfig.getProperty("screenscraper.userId"),
                RetrocenterConfig.getProperty("screenscraper.userPassword"),
                RetrocenterConfig.getProperty("screenscraper.devId"),
                RetrocenterConfig.getProperty("screenscraper.devPassword"),
                RetrocenterConfig.getProperty("screenscraper.softName")
        );
    }

    public List<String> getMediaPreferedMix() {
        return toList(RetrocenterConfig.getProperty("screenscraper.prefered.media.mix"));
    }

    public List<String> getMediaPreferedScreenshot() {
        return toList(RetrocenterConfig.getProperty("screenscraper.prefered.media.screenshot"));
    }

    public List<String> getMediaPreferedBoxart() {
        return toList(RetrocenterConfig.getProperty("screenscraper.prefered.media.boxart"));
    }

    public List<String> getMediaPreferedCartridge() {
        return toList(RetrocenterConfig.getProperty("screenscraper.prefered.media.cartridge"));
    }

    public List<String> getMediaPreferedFanart() {
        return toList(RetrocenterConfig.getProperty("screenscraper.prefered.media.fanart"));
    }

    public List<String> getMediaPreferedManual() {
        return toList(RetrocenterConfig.getProperty("screenscraper.prefered.media.manual"));
    }

    public List<String> getMediaPreferedWheel() {
        return toList(RetrocenterConfig.getProperty("screenscraper.prefered.media.wheel"));
    }

    public List<String> getMediaPreferedMarquee() {
        return toList(RetrocenterConfig.getProperty("screenscraper.prefered.media.marquee"));
    }

    public List<String> getMediaPreferedVideo() {
        return toList(RetrocenterConfig.getProperty("screenscraper.prefered.media.video"));
    }

    public List<String> getMediaPreferedImage() {
        return toList(RetrocenterConfig.getProperty("screenscraper.useras.media.image"));
    }

    public boolean isMediaEnabledMix() {
        return toBoolean(RetrocenterConfig.getProperty("screenscraper.download.media.mix"));
    }

    public boolean isMediaEnabledScreenshot() {
        return toBoolean(RetrocenterConfig.getProperty("screenscraper.download.media.screenshot"));
    }

    public boolean isMediaEnabledBoxart() {
        return toBoolean(RetrocenterConfig.getProperty("screenscraper.download.media.boxart"));
    }

    public boolean isMediaEnabledCartridge() {
        return toBoolean(RetrocenterConfig.getProperty("screenscraper.download.media.cartridge"));
    }

    public boolean isMediaEnabledFanart() {
        return toBoolean(RetrocenterConfig.getProperty("screenscraper.download.media.fanart"));
    }

    public boolean isMediaEnabledManual() {
        return toBoolean(RetrocenterConfig.getProperty("screenscraper.download.media.manual"));
    }

    public boolean isMediaEnabledWheel() {
        return toBoolean(RetrocenterConfig.getProperty("screenscraper.download.media.wheel"));
    }

    public boolean isMediaEnabledMarquee() {
        return toBoolean(RetrocenterConfig.getProperty("screenscraper.download.media.marquee"));
    }

    public boolean isMediaEnabledVideo() {
        return toBoolean(RetrocenterConfig.getProperty("screenscraper.download.media.video"));
    }

    public List<String> getPreferedRegion() {
        return toList(RetrocenterConfig.getProperty("screenscraper.prefered.region"));
    }

    public List<String> getPreferedLanguage() {
        return toList(RetrocenterConfig.getProperty("screenscraper.prefered.language"));
    }

    private boolean toBoolean(String value) {
        return value != null && (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("yes"));
    }

    private List<String> toList(String values) {
        List<String> result = new LinkedList<>();
        for (String s : values.split(",")) {
            result.add(s);
        }
        return result;
    }
}

