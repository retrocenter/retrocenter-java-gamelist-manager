package com.retrocenter.gamelist.manager.manager;

public class PlatformItem {
    private String name;
    private String description;
    private Long screenscraperId;

    public PlatformItem() {
    }

    public PlatformItem(String name, String description, Long screenscraperId) {
        this.name = name;
        this.description = description;
        this.screenscraperId = screenscraperId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getScreenscraperId() {
        return screenscraperId;
    }

    public void setScreenscraperId(Long screenscraperId) {
        this.screenscraperId = screenscraperId;
    }

    @Override
    public String toString() {
        return "PlatformItem{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", screenscraperId=" + screenscraperId +
                '}';
    }
}
