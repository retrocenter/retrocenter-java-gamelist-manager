package com.retrocenter.gamelist.manager.manager;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import com.retrocenter.datafile.retrocenterdb.RetrocenterDB;
import com.retrocenter.datafile.retrocenterdb.RetrocenterGame;
import com.retrocenter.gamelist.manager.RetrocenterConfig;
import com.retrocenter.gamelist.manager.audit.AuditGameList;
import com.retrocenter.gamelist.manager.audit.AuditedGame;
import com.retrocenter.gamelist.manager.util.GamelistDatabase;
import com.retrocenter.gamelist.manager.util.PlatformUtil;
import com.retrocenter.gamelist.manager.util.RetrocenterServerUtil;

public class GamelistManagerUI extends JFrame {
    private static String LABEL_SCREENSCRAPPER_START = "Screenscraper Start";
    private static String LABEL_SCREENSCRAPPER_STOP = "Screenscraper Stop";
    private static String LABEL_FIND_ID_START = "Find ID Start";
    private static String LABEL_FIND_ID_STOP = "Find ID Stop";

    private JComboBox cbPlatform;
    private JTable jtGames;
    private JPopupMenu popupMenu;
    private SwingWorker screenscraperWorker = null;
    private SwingWorker findIdWorker = null;

    private JButton btScreenscraper;
    private JButton btFindID;

    public GamelistManagerUI() {
        super("Gamelist Manager");
        createContent();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                GamelistManagerUI win = new GamelistManagerUI();
                win.setSize(1200, 800);
                win.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                win.setVisible(true);
            }
        });
    }

    private void createContent() {
        getContentPane().setLayout(new BorderLayout());

        getContentPane().add(createTopContent(), BorderLayout.NORTH);
        getContentPane().add(createCenterContent(), BorderLayout.CENTER);
        //        getContentPane().add(createRightContent(), BorderLayout.EAST);
    }

    private JPanel createTopContent() {
        JPanel pnTop = new JPanel();
        pnTop.setLayout(new BorderLayout());

        JPanel top = new JPanel();
        pnTop.add(top, BorderLayout.NORTH);

        top.setLayout(new FlowLayout());
        JLabel lb = new JLabel("Platform: ");
        top.add(lb);

        cbPlatform = new JComboBox();
        cbPlatform.addItem(" ");
        for (PlatformItem p : PlatformUtil.listPlatforms()) {
            cbPlatform.addItem(p.getName());
        }
        top.add(cbPlatform);
        cbPlatform.addItemListener(e -> {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                fillTableContent(e.getItem().toString());
            }
        });

        JButton btSave = new JButton("Save");
        btSave.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveGamelist();
            }
        });
        top.add(btSave);

        JButton btRefresh = new JButton("Refresh");
        btRefresh.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fillTableContent(cbPlatform.getSelectedItem().toString());
            }
        });
        top.add(btRefresh);

        JPanel botton = new JPanel();
        botton.setLayout(new FlowLayout());
        pnTop.add(botton, BorderLayout.SOUTH);

        btFindID = new JButton(LABEL_FIND_ID_START);
        btFindID.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fillRetrocenterID();
            }
        });
        botton.add(btFindID);

        btScreenscraper = new JButton(LABEL_SCREENSCRAPPER_START);
        btScreenscraper.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                screenScraper();
            }
        });
        botton.add(btScreenscraper);

        //        JButton btAutoFillImage = new JButton("Auto fill image");
        //        btAutoFillImage.addActionListener(new AbstractAction() {
        //            @Override
        //            public void actionPerformed(ActionEvent e) {
        //                autoFillImage();
        //            }
        //        });
        //        botton.add(btAutoFillImage);

        //        JButton btAutoFillName = new JButton("Auto fill names");
        //        btAutoFillName.addActionListener(new AbstractAction() {
        //            @Override
        //            public void actionPerformed(ActionEvent e) {
        //                autoFillName();
        //            }
        //        });
        //        botton.add(btAutoFillName);

        //        JButton btMoveMedia = new JButton("Auto move media files");
        //        btMoveMedia.addActionListener(new AbstractAction() {
        //            @Override
        //            public void actionPerformed(ActionEvent e) {
        //                //TODO:                moveMediaToCorrectFolder();
        //            }
        //        });
        //        botton.add(btMoveMedia);

        //        JButton btCreateClearList = new JButton("Create clear list");
        //        btCreateClearList.addActionListener(new AbstractAction() {
        //            @Override
        //            public void actionPerformed(ActionEvent e) {
        //                createClearList();
        //            }
        //        });
        //        botton.add(btCreateClearList);

        return pnTop;
    }

    private JPanel createCenterContent() {
        JPanel jp = new JPanel();
        jp.setLayout(new BorderLayout());

        ManagerGameTableModel model = new ManagerGameTableModel();
        jtGames = new JTable(model);
        jtGames.setFont(new Font("Dialog", Font.PLAIN, 18));
        jtGames.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        jtGames.setRowHeight(jtGames.getFont().getSize() + 4);
        jtGames.setDefaultRenderer(String.class, new AuditedGameTableCellRenderer());
        jtGames.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        jtGames.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                Point point = e.getPoint();
                int currentRow = jtGames.rowAtPoint(point);
                jtGames.setRowSelectionInterval(currentRow, currentRow);
            }
        });

        popupMenu = new JPopupMenu();
        JMenuItem miRemoveGame = new JMenuItem("Remove Game");
        miRemoveGame.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeGame();
            }
        });
        popupMenu.add(miRemoveGame);
        jtGames.setComponentPopupMenu(popupMenu);

        JScrollPane scrollPane = new JScrollPane(jtGames);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        jp.add(scrollPane, BorderLayout.CENTER);
        for (int i = 0; i < jtGames.getColumnCount(); i++) {
            int width;
            switch (ManagerGameTableModel.COLUMNS[i]) {
                case ManagerGameTableModel.COLUMN_ID:
                    width = 70;
                    break;
                case ManagerGameTableModel.COLUMN_IS_GAME:
                case ManagerGameTableModel.COLUMN_HACK:
                case ManagerGameTableModel.COLUMN_PROTO:
                case ManagerGameTableModel.COLUMN_BETA:
                case ManagerGameTableModel.COLUMN_DEMO:
                case ManagerGameTableModel.COLUMN_UNLICENSED:
                case ManagerGameTableModel.COLUMN_ALTERNATE:
                case ManagerGameTableModel.COLUMN_TRANSLATED:
                case ManagerGameTableModel.COLUMN_IS_CLONE:
                    width = 60;
                    break;
                default:
                    width = 200;
            }
            jtGames.getColumnModel().getColumn(i).setPreferredWidth(width);
        }

        return jp;
    }

    private void fillTableContent(String p) {
        List<AuditedGame> games = new LinkedList<>();
        System.out.println("Finding games for plataform: " + p);
        if (p != null && !p.trim().isEmpty()) {
            try {
                RetrocenterDB gameList = GamelistDatabase.readRetrocenterDB(new File(RetrocenterConfig.getGamelistBaseDestDir(p)));
                games = AuditGameList.auditGameList(RetrocenterConfig.getROMsBaseDestDir(), RetrocenterConfig.getMediaBaseDestDir(p), p, gameList);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        ((ManagerGameTableModel) jtGames.getModel()).setSystemName(p);
        ((ManagerGameTableModel) jtGames.getModel()).setValues(games);
    }

    private void removeGame() {
        ManagerGameTableModel model = ((ManagerGameTableModel) jtGames.getModel());
        model.getValues().remove(jtGames.getSelectedRow());
        model.fireTableDataChanged();
    }

    private void saveGamelist() {
        System.out.println("Saving Retrocenter DB");
        Object p = cbPlatform.getSelectedItem();
        if (p != null && !p.toString().isEmpty()) {
            ManagerGameTableModel model = ((ManagerGameTableModel) jtGames.getModel());
            RetrocenterDB gl = new RetrocenterDB();
            for (AuditedGame g : model.getValues()) {
                gl.addGame(g.getGame());
            }
            try {
                GamelistDatabase.saveRetrocenterDB(gl, new File(RetrocenterConfig.getGamelistBaseDestDir(p.toString())));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void screenScraper() {
        if (screenscraperWorker == null) {
            screenscraperWorker = new SwingWorker() {
                @Override
                protected Object doInBackground() {
                    ScreenscraperThread screenscraperThread = new ScreenscraperThread(((ManagerGameTableModel) jtGames.getModel()).getValues());
                    screenscraperThread.execute();
                    return null;
                }

                @Override
                protected void done() {
                    screenscraperWorker = null;
                    btScreenscraper.setText(LABEL_SCREENSCRAPPER_START);
                    ManagerGameTableModel model = ((ManagerGameTableModel) jtGames.getModel());
                    model.fireTableDataChanged();
                }
            };
            screenscraperWorker.execute();
            btScreenscraper.setText(LABEL_SCREENSCRAPPER_STOP);
        } else {
            if (screenscraperWorker != null) {
                screenscraperWorker.cancel(true);
            }
        }
    }

    private void fillRetrocenterID() {
        if (findIdWorker == null) {
            findIdWorker = new SwingWorker() {
                @Override
                protected Object doInBackground() throws Exception {
                    System.out.println("Fillin retrocenter ID");
                    ManagerGameTableModel model = ((ManagerGameTableModel) jtGames.getModel());
                    for (AuditedGame game : model.getValues()) {
                        RetrocenterGame g = game.getGame();
                        if (g.getRetrocenterID() == null) {
                            System.out.println(" - Buscando retrocenter ID para " + g.getPath());
                            try {
                                String id = RetrocenterServerUtil.findGameID(game.getGame());
                                System.out.println("  -- ID=" + id);
                                game.getGame().setRetrocenterID(id);
                            } catch (Exception exception) {
                                exception.printStackTrace();
                            }
                        }
                    }
                    return null;
                }

                @Override
                protected void done() {
                    findIdWorker = null;
                    btFindID.setText(LABEL_FIND_ID_START);
                    ManagerGameTableModel model = ((ManagerGameTableModel) jtGames.getModel());
                    model.fireTableDataChanged();
                }
            };
            findIdWorker.execute();
            btFindID.setText(LABEL_FIND_ID_STOP);
        } else {
            if (findIdWorker != null) {
                findIdWorker.cancel(true);
            }
        }
    }

    //    private void autoFillImage() {
    //        System.out.println("Fillin image fields");
    //        Object p = cbPlatform.getSelectedItem();
    //        if (p != null && !p.toString().isEmpty()) {
    //            ManagerGameTableModel model = ((ManagerGameTableModel) jtGames.getModel());
    //            for (AuditedGame game : model.getValues()) {
    //                if ((game.getFileNameStatus() & GameFileNameStatus.EMPTY) == 0) {
    //                    String baseName = RomUtil.removeFileExtension(game.getGame().getPath());
    //                    baseName = baseName.substring(baseName.lastIndexOf('/') + 1);
    //                    if ((game.getImageStatus() & GameImageStatus.EMPTY) > 0) {
    //                        game.getGame().setImage(new RetrocenterMedia().setPath("./boxart/" + baseName + ".png"));
    //                    }
    //                    if ((game.getThumbnailStatus() & GameThumbnailStatus.EMPTY) > 0) {
    //                        game.getGame().setThumbnail(new RetrocenterMedia().setPath("./mixart/" + baseName + ".png"));
    //                    }
    //                    if ((game.getMarqueeStatus() & GameMarqueeStatus.EMPTY) > 0) {
    //                        game.getGame().setMarquee(new RetrocenterMedia().setPath("./marquee/" + baseName + ".png"));
    //                    }
    //                    if ((game.getVideoStatus() & GameVideoStatus.EMPTY) > 0) {
    //                        game.getGame().setVideo(new RetrocenterMedia().setPath("./video/" + baseName + ".mp4"));
    //                    } else {
    //                        if (game.getGame().getVideo().getPath().endsWith(".png")) {
    //                            game.getGame().setVideo(new RetrocenterMedia().setPath(game.getGame().getVideo().getPath().replace(".png", ".mp4")));
    //                        }
    //                    }
    //                }
    //            }
    //            model.fireTableDataChanged();
    //        }
    //    }

    //    private void autoFillName() {
    //        System.out.println("Fillin name field");
    //        Object p = cbPlatform.getSelectedItem();
    //        if (p != null && !p.toString().isEmpty()) {
    //            ManagerGameTableModel model = ((ManagerGameTableModel) jtGames.getModel());
    //            for (AuditedGame game : model.getValues()) {
    //                if ((game.getNameStatus() & GameNameStatus.EMPTY) > 0) {
    //                    if ((game.getFileNameStatus() & GameFileNameStatus.EMPTY) == 0) {
    //                        String name = RomUtil.removeFileExtension(game.getGame().getPath());
    //                        int pos = name.indexOf("(");
    //                        if (pos > 0) {
    //                            String s = name.substring(pos);
    //                            if (s.startsWith("(USA")
    //                                    || s.startsWith("(Europe")
    //                                    || s.startsWith("(Japan")
    //                                    || s.startsWith("(Netherlands")
    //                                    || s.startsWith("(France")
    //                                    || s.startsWith("(Australia")
    //                                    || s.startsWith("(Germany")
    //                                    || s.startsWith("(World")
    //                                    || s.startsWith("(UK")
    //                                    || s.startsWith("(U)")
    //                                    || s.startsWith("(Korea")
    //                                    || s.startsWith("(JPA")
    //                                    || s.startsWith("(Italy")
    //                                    || s.startsWith("(Ireland")
    //                            ) {
    //                                name = name.substring(0, pos - 1).trim();
    //                            }
    //                        }
    //
    //                        game.getGame().setName(name);
    //                    }
    //                }
    //            }
    //            model.fireTableDataChanged();
    //        }
    //    }

    //    public void createClearList() {
    //        System.out.println("Creating a clear list");
    //        Object p = cbPlatform.getSelectedItem();
    //        if (p != null && !p.toString().isEmpty()) {
    //            ManagerGameTableModel model = ((ManagerGameTableModel) jtGames.getModel());
    //            RetrocenterDB gl = new RetrocenterDB();
    //            for (AuditedGame audited : model.getValues()) {
    //                if ((audited.getNameStatus() & GameNameStatus.EMPTY) == 0
    //                        && (audited.getFileNameStatus() & GameFileNameStatus.EMPTY) == 0
    //                        && (audited.getFileNameStatus() & GameFileNameStatus.NOT_FOUND) == 0) {
    //                    gl.addGame(audited.getGame());
    //                }
    //            }
    //            try {
    //                Files.write(Paths.get(RetrocenterConfig.getMediaBaseDestDir(p.toString()), RetrocenterConfig.GAMELIST_DB_CLEAR_FILE),
    //                        gl.toString().getBytes());
    //            } catch (IOException e) {
    //                e.printStackTrace();
    //            }
    //        }
    //    }

    //    public void moveMediaToCorrectFolder() {
    //        System.out.println("Moving media");
    //        Object p = cbPlatform.getSelectedItem();
    //        if (p != null && !p.toString().isEmpty()) {
    //            ManagerGameTableModel model = ((ManagerGameTableModel) jtGames.getModel());
    //            try {
    //                for (AuditedGame game : model.getValues()) {
    //                    String destName = "./images" + game.getGame().getImage().substring(game.getGame().getImage().lastIndexOf('/'));
    //                    if ((game.getImageStatus() & GameImageStatus.OK) > 0) {
    //                        if (!game.getGame().getImage().startsWith("./images/")) {
    //                            Path from = Paths.get(RetroPieConfig.MEDIA_BASE_DIR, p.toString(), game.getGame().getImage());
    //                            Path to = Paths.get(RetroPieConfig.MEDIA_BASE_DIR, p.toString(), destName);
    //                            System.out.println("Moving [" + from + "] to [" + to + "]");
    //                            to.toFile().getParentFile().mkdirs();
    //                            if (from.toFile().exists()) {
    //                                Files.move(from, to, StandardCopyOption.REPLACE_EXISTING);
    //                            }
    //                        }
    //                    }
    //                    game.getGame().setImage(destName);
    //
    //                    destName = "./thumb" + game.getGame().getThumbnail().substring(game.getGame().getThumbnail().lastIndexOf('/'));
    //                    if ((game.getThumbnailStatus() & GameThumbnailStatus.OK) > 0) {
    //                        if (!game.getGame().getThumbnail().startsWith("./thumb/")) {
    //                            Path from = Paths.get(RetroPieConfig.MEDIA_BASE_DIR, p.toString(), game.getGame().getThumbnail());
    //                            Path to = Paths.get(RetroPieConfig.MEDIA_BASE_DIR, p.toString(), destName);
    //                            System.out.println("Moving [" + from + "] to [" + to + "]");
    //                            to.toFile().getParentFile().mkdirs();
    //                            if (from.toFile().exists()) {
    //                                Files.move(from, to, StandardCopyOption.REPLACE_EXISTING);
    //                            }
    //                        }
    //                    }
    //                    game.getGame().setThumbnail(destName);
    //
    //                    destName = "./marquee" + game.getGame().getMarquee().substring(game.getGame().getMarquee().lastIndexOf('/'));
    //                    if ((game.getMarqueeStatus() & GameMarqueeStatus.OK) > 0) {
    //                        if (!game.getGame().getMarquee().startsWith("./marquee/")) {
    //                            Path from = Paths.get(RetroPieConfig.MEDIA_BASE_DIR, p.toString(), game.getGame().getMarquee());
    //                            Path to = Paths.get(RetroPieConfig.MEDIA_BASE_DIR, p.toString(), destName);
    //                            System.out.println("Moving [" + from + "] to [" + to + "]");
    //                            to.toFile().getParentFile().mkdirs();
    //                            if (from.toFile().exists()) {
    //                                Files.move(from, to, StandardCopyOption.REPLACE_EXISTING);
    //                            }
    //                        }
    //                    }
    //                    game.getGame().setMarquee(destName);
    //
    //                    destName = "./video" + game.getGame().getVideo().substring(game.getGame().getVideo().lastIndexOf('/'));
    //                    if ((game.getVideoStatus() & GameVideoStatus.OK) > 0) {
    //                        if (!game.getGame().getVideo().startsWith("./video/")) {
    //                            Path from = Paths.get(RetroPieConfig.MEDIA_BASE_DIR, p.toString(), game.getGame().getVideo());
    //                            Path to = Paths.get(RetroPieConfig.MEDIA_BASE_DIR, p.toString(), destName);
    //                            System.out.println("Moving [" + from + "] to [" + to + "]");
    //                            to.toFile().getParentFile().mkdirs();
    //                            if (from.toFile().exists()) {
    //                                Files.move(from, to, StandardCopyOption.REPLACE_EXISTING);
    //                            }
    //                        }
    //                    }
    //                    game.getGame().setVideo(destName);
    //                }
    //            } catch (Exception ex) {
    //                ex.printStackTrace();
    //            }
    //            model.fireTableDataChanged();
    //        }
    //    }
}
