package com.retrocenter.gamelist.manager.manager;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import com.retrocenter.gamelist.manager.audit.AuditedGame;
import com.retrocenter.gamelist.manager.audit.GameCRCStatus;
import com.retrocenter.gamelist.manager.audit.GameFileNameStatus;
import com.retrocenter.gamelist.manager.audit.GameImageStatus;
import com.retrocenter.gamelist.manager.audit.GameMarqueeStatus;
import com.retrocenter.gamelist.manager.audit.GameNameStatus;
import com.retrocenter.gamelist.manager.audit.GameThumbnailStatus;
import com.retrocenter.gamelist.manager.audit.GameVideoStatus;

public class AuditedGameTableCellRenderer extends JLabel implements TableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        JLabel cell = new JLabel(value != null ? value.toString() : null);
        cell.setOpaque(true);
        Color background = Color.WHITE;
        Color foreground = Color.BLACK;

        AuditedGame game = null;
        if (row >= 0) {
            ManagerGameTableModel model = ((ManagerGameTableModel) table.getModel());
            game = model.getValues().get(row);
        }

        if (game != null) {
            switch (ManagerGameTableModel.COLUMNS[column]) {
                case ManagerGameTableModel.COLUMN_ID:
                    if (game.getRetrocenterID() == null) {
                        background = Color.YELLOW;
                    }
                    break;
                case ManagerGameTableModel.COLUMN_NAME:
                    if ((game.getNameStatus() & GameNameStatus.OK) == 0) {
                        background = Color.RED;
                    }
                    break;
                case ManagerGameTableModel.COLUMN_FILE_NAME:
                    if ((game.getFileNameStatus() & GameFileNameStatus.OK) == 0) {
                        background = Color.RED;
                    }
                    break;
                case ManagerGameTableModel.COLUMN_IS_GAME:
                    if (game.isGame() == null) {
                        background = Color.YELLOW;
                    } else if (game.isGame().booleanValue()) {
                        background = Color.GREEN;
                    }
                    break;
                case ManagerGameTableModel.COLUMN_HACK:
                    if (game.isHack() == null) {
                        background = Color.YELLOW;
                    } else if (game.isHack().booleanValue()) {
                        background = Color.GREEN;
                    }
                    break;
                case ManagerGameTableModel.COLUMN_PROTO:
                    if (game.isProto() == null) {
                        background = Color.YELLOW;
                    } else if (game.isProto().booleanValue()) {
                        background = Color.GREEN;
                    }
                    break;
                case ManagerGameTableModel.COLUMN_BETA:
                    if (game.isBeta() == null) {
                        background = Color.YELLOW;
                    } else if (game.isBeta().booleanValue()) {
                        background = Color.GREEN;
                    }
                    break;
                case ManagerGameTableModel.COLUMN_DEMO:
                    if (game.isDemo() == null) {
                        background = Color.YELLOW;
                    } else if (game.isDemo().booleanValue()) {
                        background = Color.GREEN;
                    }
                    break;
                case ManagerGameTableModel.COLUMN_UNLICENSED:
                    if (game.isUnlicensed() == null) {
                        background = Color.YELLOW;
                    } else if (game.isUnlicensed().booleanValue()) {
                        background = Color.GREEN;
                    }
                    break;
                case ManagerGameTableModel.COLUMN_ALTERNATE:
                    if (game.isAlternate() == null) {
                        background = Color.YELLOW;
                    } else if (game.isAlternate().booleanValue()) {
                        background = Color.GREEN;
                    }
                    break;
                case ManagerGameTableModel.COLUMN_TRANSLATED:
                    if (game.isTranslated() == null) {
                        background = Color.YELLOW;
                    } else if (game.isTranslated().booleanValue()) {
                        background = Color.GREEN;
                    }
                    break;
                case ManagerGameTableModel.COLUMN_IS_CLONE:
                    if (game.isClone() == null) {
                        background = Color.YELLOW;
                    } else if (game.isClone().booleanValue()) {
                        background = Color.GREEN;
                    }
                    break;
                case ManagerGameTableModel.COLUMN_IMAGE_NAME:
                    if ((game.getImageStatus() & GameImageStatus.OK) == 0) {
                        background = Color.YELLOW;
                    }
                    break;
                case ManagerGameTableModel.COLUMN_THUMBNAIL_NAME:
                    if ((game.getThumbnailStatus() & GameThumbnailStatus.OK) == 0) {
                        background = Color.YELLOW;
                    }
                    break;
                case ManagerGameTableModel.COLUMN_MARQUEE_NAME:
                    if ((game.getMarqueeStatus() & GameMarqueeStatus.OK) == 0) {
                        background = Color.YELLOW;
                    }
                    break;
                case ManagerGameTableModel.COLUMN_VIDEO_NAME:
                    if ((game.getVideoStatus() & GameVideoStatus.OK) == 0) {
                        background = Color.YELLOW;
                    }
                    break;
                case ManagerGameTableModel.COLUMN_CRC:
                    if ((game.getCrcStatus() & GameCRCStatus.OK) == 0) {
                        background = Color.RED;
                    }
                    break;
            }
        }

        cell.setBackground(isSelected ? foreground : background);
        cell.setForeground(isSelected ? background : foreground);
        return cell;
    }
}
