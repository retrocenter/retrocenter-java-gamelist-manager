package com.retrocenter.gamelist.manager.manager;

import java.util.LinkedList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import com.retrocenter.datafile.retrocenterdb.RetrocenterGame;
import com.retrocenter.gamelist.manager.audit.AuditedGame;
import com.retrocenter.gamelist.manager.audit.GameCRCStatus;
import com.retrocenter.gamelist.manager.audit.GameFileNameStatus;
import com.retrocenter.gamelist.manager.audit.GameImageStatus;
import com.retrocenter.gamelist.manager.audit.GameMarqueeStatus;
import com.retrocenter.gamelist.manager.audit.GameNameStatus;
import com.retrocenter.gamelist.manager.audit.GameThumbnailStatus;
import com.retrocenter.gamelist.manager.audit.GameVideoStatus;

public class ManagerGameTableModel extends AbstractTableModel {
    public static final String COLUMN_ID = "ID";
    public static final String COLUMN_NAME = "Name";
    public static final String COLUMN_FILE_NAME = "File name";

    public static final String COLUMN_IS_GAME = "Is Game?";
    public static final String COLUMN_HACK = "Hack?";
    public static final String COLUMN_PROTO = "Proto?";
    public static final String COLUMN_BETA = "Beta?";
    public static final String COLUMN_DEMO = "Demo?";
    public static final String COLUMN_UNLICENSED = "Unl.?";
    public static final String COLUMN_ALTERNATE = "Alt.?";
    public static final String COLUMN_TRANSLATED = "Translated?";
    public static final String COLUMN_IS_CLONE = "Is Clone?";

    public static final String COLUMN_IMAGE_NAME = "Image name";
    public static final String COLUMN_THUMBNAIL_NAME = "Thumbnail";
    public static final String COLUMN_MARQUEE_NAME = "Marquee";
    public static final String COLUMN_VIDEO_NAME = "Video";
    public static final String COLUMN_CRC = "CRC";

    public static final String[] COLUMNS = {COLUMN_ID,
            COLUMN_NAME,
            COLUMN_FILE_NAME,
            COLUMN_IS_GAME,
            COLUMN_HACK,
            COLUMN_PROTO,
            COLUMN_BETA,
            COLUMN_DEMO,
            COLUMN_UNLICENSED,
            COLUMN_ALTERNATE,
            COLUMN_TRANSLATED,
            COLUMN_IS_CLONE,
            COLUMN_IMAGE_NAME,
            COLUMN_THUMBNAIL_NAME,
            COLUMN_MARQUEE_NAME,
            COLUMN_VIDEO_NAME,
            COLUMN_CRC};

    private List<AuditedGame> values = new LinkedList<>();
    private String systemName;

    @Override
    public int getRowCount() {
        return values.size();
    }

    @Override
    public int getColumnCount() {
        return COLUMNS.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (rowIndex >= 0 && rowIndex < values.size()) {
            AuditedGame g = values.get(rowIndex);
            RetrocenterGame game = g.getGame();
            switch (columnIndex) {
                case 0:
                    return game.getRetrocenterID() != null ? game.getRetrocenterID() : "";
                case 1:
                    if ((g.getNameStatus() & GameNameStatus.DUPLICATED) > 0) {
                        return "[DUPL] " + game.getName();
                    }
                    return game.getName();
                case 2:
                    StringBuilder path = new StringBuilder();
                    if ((g.getFileNameStatus() & GameFileNameStatus.DUPLICATED) > 0) {
                        path.append("[DUPL] ");
                    }
                    if ((g.getFileNameStatus() & GameFileNameStatus.NOT_FOUND) > 0) {
                        path.append("[NOT_FOUND] ");
                    }
                    path.append(game.getPath());
                    return path.toString();
                case 3:
                    return g.isGame() != null ? g.isGame().toString() : "";
                case 4:
                    return g.isHack() != null ? g.isHack().toString() : "";
                case 5:
                    return g.isProto() != null ? g.isProto().toString() : "";
                case 6:
                    return g.isBeta() != null ? g.isBeta().toString() : "";
                case 7:
                    return g.isDemo() != null ? g.isDemo().toString() : "";
                case 8:
                    return g.isUnlicensed() != null ? g.isUnlicensed().toString() : "";
                case 9:
                    return g.isAlternate() != null ? g.isAlternate().toString() : "";
                case 10:
                    return g.isTranslated() != null ? g.isTranslated().toString() : "";
                case 11:
                    return g.isClone() != null ? g.isClone().toString() : "";
                case 12:
                    if ((g.getImageStatus() & GameImageStatus.NOT_FOUND) > 0) {
                        return "[NOT FOUND] " + game.getImage();
                    }
                    return game.getImage() != null ? game.getImage().getPath() : "";
                case 13:
                    if ((g.getThumbnailStatus() & GameThumbnailStatus.NOT_FOUND) > 0) {
                        return "[NOT FOUND] " + game.getThumbnail();
                    }
                    return game.getThumbnail() != null ? game.getThumbnail().getPath() : "";
                case 14:
                    if ((g.getMarqueeStatus() & GameMarqueeStatus.NOT_FOUND) > 0) {
                        return "[NOT FOUND] " + game.getMarquee();
                    }
                    return game.getMarquee() != null ? game.getMarquee().getPath() : "";
                case 15:
                    if ((g.getVideoStatus() & GameVideoStatus.NOT_FOUND) > 0) {
                        return "[NOT FOUND] " + game.getVideo();
                    }
                    return game.getVideo() != null ? game.getVideo().getPath() : "";
                case 16:
                    StringBuilder crc = new StringBuilder();
                    if ((g.getCrcStatus() & GameCRCStatus.DUPLICATED) > 0) {
                        crc.append("[DUPL] ");
                    }
                    if ((g.getCrcStatus() & GameCRCStatus.NOT_FOUND) > 0) {
                        crc.append("[NOT_FOUND] ");
                    }
                    if ((g.getCrcStatus() & GameCRCStatus.MULTI) > 0) {
                        crc.append("[MULTI] ");
                    }
                    if (g.getCrc().size() > 0) {
                        crc.append(g.getCrc().keySet());
                    }
                    return crc.toString();
                default:
                    return null;
            }
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public String getColumnName(int column) {
        return COLUMNS[column];
    }

    public List<AuditedGame> getValues() {
        return values;
    }

    public void setValues(List<AuditedGame> values) {
        this.values = values;
        fireTableDataChanged();
    }

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }
}
