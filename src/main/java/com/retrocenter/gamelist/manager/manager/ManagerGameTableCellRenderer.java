package com.retrocenter.gamelist.manager.manager;

import java.awt.Color;
import java.awt.Component;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import com.retrocenter.gamelist.manager.RetrocenterConfig;

public class ManagerGameTableCellRenderer extends JLabel implements TableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        JLabel cell = new JLabel(value != null ? value.toString() : null);
        cell.setOpaque(true);
        Color background = Color.WHITE;
        Color foreground = Color.BLACK;
        //TODO: Implementar
        if (table.getColumnName(column).equals(ManagerGameTableModel.COLUMN_FILE_NAME)) {
            if (value != null && !value.toString().trim().isEmpty()) {
                Path path = Paths.get(RetrocenterConfig.getROMsBaseDestDir(), ((ManagerGameTableModel) table.getModel()).getSystemName(), value.toString());
                foreground = Color.BLACK;
                background = path.toFile().exists() ? Color.GREEN : Color.RED;
            } else {
                background = Color.RED;
                foreground = Color.BLACK;
            }
        } else if (table.getColumnName(column).equals(ManagerGameTableModel.COLUMN_IMAGE_NAME) && value != null && !value.toString().trim().isEmpty()) {
            Path path = Paths.get(RetrocenterConfig.getMediaBaseDestDir(((ManagerGameTableModel) table.getModel()).getSystemName()), value.toString());
            background = path.toFile().exists() ? Color.GREEN : Color.YELLOW;
            foreground = Color.BLACK;
        } else if (table.getColumnName(column).equals(ManagerGameTableModel.COLUMN_THUMBNAIL_NAME) && value != null && !value.toString().trim().isEmpty()) {
            Path path = Paths.get(RetrocenterConfig.getMediaBaseDestDir(((ManagerGameTableModel) table.getModel()).getSystemName()), value.toString());
            background = path.toFile().exists() ? Color.GREEN : Color.YELLOW;
            foreground = Color.BLACK;
        } else if (table.getColumnName(column).equals(ManagerGameTableModel.COLUMN_MARQUEE_NAME) && value != null && !value.toString().trim().isEmpty()) {
            Path path = Paths.get(RetrocenterConfig.getMediaBaseDestDir(((ManagerGameTableModel) table.getModel()).getSystemName()), value.toString());
            background = path.toFile().exists() ? Color.GREEN : Color.YELLOW;
            foreground = Color.BLACK;
        } else if (table.getColumnName(column).equals(ManagerGameTableModel.COLUMN_VIDEO_NAME) && value != null && !value.toString().trim().isEmpty()) {
            Path path = Paths.get(RetrocenterConfig.getMediaBaseDestDir(((ManagerGameTableModel) table.getModel()).getSystemName()), value.toString());
            background = path.toFile().exists() ? Color.GREEN : Color.YELLOW;
            foreground = Color.BLACK;
        }
        cell.setBackground(isSelected ? foreground : background);
        cell.setForeground(isSelected ? background : foreground);
        return cell;
    }
}
