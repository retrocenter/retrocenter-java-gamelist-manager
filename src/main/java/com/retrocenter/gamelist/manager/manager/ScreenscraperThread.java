package com.retrocenter.gamelist.manager.manager;

import java.util.List;

import com.retrocenter.datafile.retrocenterdb.RetrocenterGame;
import com.retrocenter.datafile.retrocenterdb.RetrocenterSSGame;
import com.retrocenter.gamelist.manager.audit.AuditedGame;
import com.retrocenter.gamelist.manager.screenscraper.ScreenScraperConfig;
import com.retrocenter.screenscraper.client.request.GameInfoRequest;
import com.retrocenter.screenscraper.client.request.GameInfoRequestParams;
import com.retrocenter.screenscraper.client.serializer.SerializeAsXml;
import com.retrocenter.screenscraper.client.types.Game;
import com.retrocenter.screenscraper.client.types.Rom;
import com.retrocenter.screenscraper.client.types.TranslatedName;
import com.retrocenter.screenscraper.client.types.TranslatedSynopsis;
import com.retrocenter.screenscraper.client.types.grouper.Games;

public class ScreenscraperThread {
    private final List<AuditedGame> games;

    public ScreenscraperThread(List<AuditedGame> games) {
        this.games = games;
    }

    public void execute() {
        System.out.println("[I] Iniciando Screenscraper. Games=" + games.size());
        for (AuditedGame auditedGame : games) {
            RetrocenterGame rcGame = auditedGame.getGame();
            System.out.println(" - [I] Processando game: " + rcGame.getName() + ": " + rcGame.getCrc());
            if (rcGame.getScreenscraper() != null) {
                System.err.println("  - [W] Ignorando por já estar preenchido");
                continue;
            }

            if (rcGame.getCrc() == null) {
                //TODO: Implementar
                System.err.println("  - [E] Nao possui CRC");
                continue;
            }
            GameInfoRequestParams params = new GameInfoRequestParams().setCrc(rcGame.getCrc());
            try {
                Games games = GameInfoRequest.get(ScreenScraperConfig.getInstance().getLoginInfo(), params);
                if (games.getGames().size() == 0) {
                    System.err.println("  - [E] Nao encontrado");
                    continue;
                } else if (games.getGames().size() > 1) {
                    System.err.println("  - [E] Encontrado mais de um (" + games.getGames().size() + ")");
                    continue;
                }
                Game game = games.getGames().get(0);

                System.out.println(new SerializeAsXml().serialize(game));

                RetrocenterSSGame screenscraper = new RetrocenterSSGame().setId(game.getId());
                screenscraper.setRomId(game.getRomId());
                screenscraper.setIsGame(game.getIsGame());
                screenscraper.setCloneOf(game.getCloneOf());

                // Obtem o nome
                if (game.getNames().size() == 0) {
                    throw new RuntimeException("Não possui Nome");
                }
                if (game.getNames().size() > 1) {
                    System.err.println(game.getNames());
                    throw new RuntimeException("Possui mais de um nome");
                }
                TranslatedName tname = game.getNames().get("nom");
                if (tname == null) {
                    System.err.println(game.getNames());
                    throw new RuntimeException("Nao encontrado nome com language 'nom'");
                }
                rcGame.setName(tname.getValue());

                // Obtem a descricao
                if (game.getSynopsis().values().iterator().hasNext()) {
                    boolean achou = false;
                    for (String lang : ScreenScraperConfig.getInstance().getPreferedLanguage()) {
                        if (achou)
                            break;
                        for (TranslatedSynopsis sinop : game.getSynopsis().values()) {
                            if (sinop.getLanguageCode().equals(lang)) {
                                rcGame.setDesc(sinop.getValue());
                                achou = true;
                                break;
                            }
                        }
                    }
                    if (!achou) {
                        System.err.println("  - [E] Nao achou descricao para os idiomas preferidos");
                        System.out.println(game.getSynopsis());
                        System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
                    }
                } else {
                    System.err.println("  - [E] Nao retornou descricao");
                }

                rcGame.setPlayers(game.getPlayers());
                for (Rom rom : game.getRoms()) {
                    if (rom.getId() == game.getRomId()) {
                        screenscraper.setBeta(rom.getBeta());
                        screenscraper.setProto(rom.getProto());
                        screenscraper.setHack(rom.getHack());
                        screenscraper.setDemo(rom.getDemo());
                        screenscraper.setTranslated(rom.getTranslated());
                        screenscraper.setAlternate(rom.getAlternate());
                        screenscraper.setUnlicensed(rom.getUnlicensed());
                        break;
                    }
                }
                //                rcGame.setGenre();
                //                rcGame.setPublisher();
                //                rcGame.setDeveloper();
                //                rcGame.setReleasedate();
                //                rcGame.setRating();

                rcGame.setScreenscraper(screenscraper);
            } catch (InterruptedException ex) {
                return;
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }
}
