package com.retrocenter.gamelist.manager.audit;

import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.retrocenter.datafile.retrocenterdb.RetrocenterDB;
import com.retrocenter.datafile.retrocenterdb.RetrocenterGame;
import com.retrocenter.gamelist.manager.util.FileUtil;
import com.retrocenter.util.RomUtil;

public class AuditGameList {
    //    public static void auditGameListOld(String romsBaseDir, String systemName, GameList gameList) {
    //        for (Game game : gameList.getGames()) {
    //            if (!auditGameOld(romsBaseDir, systemName, game)) {
    //                System.out.println("@@@@ ROM Inexistente: " + game.getPath());
    //            }
    //        }
    //    }

    //    public static boolean auditGameOld(String romsBaseDir, String systemName, Game game) {
    //        if (FindGamesWithoutName.isEmptyName(game)) {
    //            System.out.println("@@@@ Sem nome: " + game.getPath());
    //        }
    //        return FindGameWithNonExistentRom.auditGame(romsBaseDir, systemName, game);
    //    }

    public static List<AuditedGame> auditGameList(String romsBaseDir, String mediaBaseDir, String systemName, RetrocenterDB gameList) throws Exception {
        List<AuditedGame> result = new LinkedList<>();

        for (RetrocenterGame game : gameList.getGames()) {
            AuditedGame auditedGame = auditGame(romsBaseDir, mediaBaseDir, systemName, game);
            result.add(auditedGame);
        }
        Collections.sort(result);

        AuditedGame needAudit;
        // Valida duplicidades
        if (result.size() > 0) {
            if (result.size() > 1) {
                // Valida duplicidade de nomes
                for (int i = 1; i < result.size(); i++) {
                    AuditedGame prev = result.get(i - 1);
                    AuditedGame game = result.get(i);

                    //Valida o nome
                    if ((game.getNameStatus() & GameNameStatus.EMPTY) == 0) {
                        if ((prev.getNameStatus() & GameNameStatus.EMPTY) == 0) {
                            if (prev.getGame().getName().trim().equalsIgnoreCase(game.getGame().getName().trim())) {
                                prev.setNameStatus(GameNameStatus.DUPLICATED);
                                game.setNameStatus(GameNameStatus.DUPLICATED);
                            } else {
                                if (prev.getNameStatus() == 0) {
                                    prev.setNameStatus(GameNameStatus.OK);
                                }
                            }
                        }
                    }
                }

                // Valida duplicidade de path (nome do arquivo) e CRC
                for (int i = 0; i < result.size(); i++) {
                    AuditedGame game = result.get(i);

                    if ((game.getFileNameStatus() & GameFileNameStatus.EMPTY) == 0) {
                        for (int j = i + 1; j < result.size(); j++) {
                            AuditedGame nextGame = result.get(j);

                            if ((nextGame.getFileNameStatus() & GameFileNameStatus.EMPTY) == 0) {
                                // Valida duplicidade de Path
                                if (RomUtil.removeFileExtension(game.getGame().getPath()).equalsIgnoreCase(RomUtil.removeFileExtension(nextGame.getGame().getPath()))) {
                                    game.setFileNameStatus((byte) (game.getFileNameStatus() | GameFileNameStatus.DUPLICATED));
                                    nextGame.setFileNameStatus((byte) (nextGame.getFileNameStatus() | GameFileNameStatus.DUPLICATED));
                                } else {
                                    if (game.getFileNameStatus() == 0) {
                                        game.setFileNameStatus(GameFileNameStatus.OK);
                                    }
                                }

                                // Valida duplicidade de CRC
                                for (String crc : game.getCrc().keySet()) {
                                    if (nextGame.getCrc().containsKey(crc)) {
                                        game.setCrcStatus((byte) (game.getCrcStatus() | GameCRCStatus.DUPLICATED));
                                        nextGame.setCrcStatus((byte) (nextGame.getCrcStatus() | GameCRCStatus.DUPLICATED));
                                        game.getGame().setCrc(null);
                                        break;
                                    }
                                }
                                if (game.getCrc().size() > 1) {
                                    game.setCrcStatus((byte) (game.getCrcStatus() | GameCRCStatus.MULTI));
                                    game.getGame().setCrc(null);
                                }
                                if (game.getCrcStatus() == 0) {
                                    game.setCrcStatus(GameCRCStatus.OK);
                                    game.getGame().setCrc(game.getCrc().keySet().iterator().next());
                                }
                            }
                        }
                    }
                }
                needAudit = result.get(result.size() - 1);
            } else {
                needAudit = result.get(0);
            }

            if (needAudit.getNameStatus() == 0) {
                needAudit.setNameStatus(GameNameStatus.OK);
            }
            if (needAudit.getFileNameStatus() == 0) {
                needAudit.setFileNameStatus(GameFileNameStatus.OK);
            }
            if (needAudit.getCrcStatus() == 0) {
                needAudit.setCrcStatus(GameCRCStatus.OK);
            }
        }

        return result;
    }

    public static AuditedGame auditGame(String romsBaseDir, String mediaBaseDir, String systemName, RetrocenterGame game) throws Exception {
        AuditedGame audited = new AuditedGame(game);
        // Valida o nome
        if (FindGamesWithoutName.isEmptyName(game)) {
            audited.setNameStatus((byte) (audited.getNameStatus() | GameNameStatus.EMPTY));
        }

        // Valida o PATH
        if (!FindGameWithNonExistentRom.isPathFilled(game)) {
            audited.setFileNameStatus((byte) (audited.getFileNameStatus() | GameFileNameStatus.EMPTY));
            audited.setCrcStatus(GameCRCStatus.EMPTY);
        } else if (!FindGameWithNonExistentRom.existRomFile(romsBaseDir, systemName, game)) {
            audited.setFileNameStatus((byte) (audited.getFileNameStatus() | GameFileNameStatus.NOT_FOUND));
            audited.setCrcStatus(GameCRCStatus.NOT_FOUND);
        } else {
            Map<Long, List<String>> crcs = FileUtil.listCRCOfFile(Paths.get(romsBaseDir, systemName, game.getPath()).toFile());
            if (crcs != null && !crcs.isEmpty()) {
                Map<String, List<String>> newCRCs = audited.getCrc();
                for (Long key : crcs.keySet()) {
                    newCRCs.put(String.format("%08X", key), crcs.get(key));
                }
                audited.setCrc(newCRCs);
            }
        }

        // Valida imagem
        if (!FindGameWithoutImage.isImageFilled(game)) {
            audited.setImageStatus(GameImageStatus.EMPTY);
        } else if (!FindGameWithoutImage.existImageFile(mediaBaseDir, systemName, game)) {
            audited.setImageStatus(GameImageStatus.NOT_FOUND);
        } else {
            audited.setImageStatus(GameImageStatus.OK);
        }

        // Valida thumbnail
        if (!FindGameWithoutThumbnail.isThumbnailFilled(game)) {
            audited.setThumbnailStatus(GameThumbnailStatus.EMPTY);
        } else if (!FindGameWithoutThumbnail.existThumbnailFile(mediaBaseDir, systemName, game)) {
            audited.setThumbnailStatus(GameThumbnailStatus.NOT_FOUND);
        } else {
            audited.setThumbnailStatus(GameThumbnailStatus.OK);
        }

        // Valida Marquee
        if (!FindGameWithoutMarquee.isMarqueeFilled(game)) {
            audited.setMarqueeStatus(GameMarqueeStatus.EMPTY);
        } else if (!FindGameWithoutMarquee.existMarqueeFile(mediaBaseDir, systemName, game)) {
            audited.setMarqueeStatus(GameMarqueeStatus.NOT_FOUND);
        } else {
            audited.setMarqueeStatus(GameMarqueeStatus.OK);
        }

        // Valida video
        if (!FindGameWithoutVideo.isVideoFilled(game)) {
            audited.setVideoStatus(GameVideoStatus.EMPTY);
        } else if (!FindGameWithoutVideo.existVideoFile(mediaBaseDir, systemName, game)) {
            audited.setVideoStatus(GameVideoStatus.NOT_FOUND);
        } else {
            audited.setVideoStatus(GameVideoStatus.OK);
        }

        return audited;
    }
}
