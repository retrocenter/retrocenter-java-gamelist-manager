package com.retrocenter.gamelist.manager.audit;

import java.io.File;
import java.nio.file.Paths;

import com.retrocenter.datafile.retrocenterdb.RetrocenterGame;

public final class FindGameWithoutVideo {
    public static boolean isVideoFilled(RetrocenterGame game) {
        if (game.getVideo() == null || game.getVideo().getPath().trim().isEmpty()) {
            return false;
        }
        return true;
    }

    public static boolean existVideoFile(String mediaBaseDir, String systemName, RetrocenterGame game) {
        String path = game.getVideo().getPath();
        if (path.startsWith("./"))
            path = path.substring(2);
        File video = Paths.get(mediaBaseDir, systemName, path).toFile();
        return video.exists();
    }
}
