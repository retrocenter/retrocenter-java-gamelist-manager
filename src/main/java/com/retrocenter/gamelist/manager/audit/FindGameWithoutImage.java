package com.retrocenter.gamelist.manager.audit;

import java.io.File;
import java.nio.file.Paths;

import com.retrocenter.datafile.retrocenterdb.RetrocenterGame;

public final class FindGameWithoutImage {
    public static boolean isImageFilled(RetrocenterGame game) {
        if (game.getImage() == null || game.getImage().getPath().trim().isEmpty()) {
            return false;
        }
        return true;
    }

    public static boolean existImageFile(String mediaBaseDir, String systemName, RetrocenterGame game) {
        String path = game.getImage().getPath();
        if (path.startsWith("./"))
            path = path.substring(2);
        File image = Paths.get(mediaBaseDir, systemName, path).toFile();
        return image.exists();
    }
}
