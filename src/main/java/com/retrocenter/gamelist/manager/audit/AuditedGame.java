package com.retrocenter.gamelist.manager.audit;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.retrocenter.datafile.retrocenterdb.RetrocenterGame;

public class AuditedGame implements Comparable<AuditedGame> {
    private RetrocenterGame game;

    private Map<String, List<String>> crc = new HashMap<>();
    private byte nameStatus = 0;
    private byte fileNameStatus = 0;
    private byte imageStatus = 0;
    private byte thumbnailStatus = 0;
    private byte marqueeStatus = 0;
    private byte videoStatus = 0;
    private byte crcStatus = 0;

    public AuditedGame(RetrocenterGame game) {
        this.game = game;
    }

    public RetrocenterGame getGame() {
        return game;
    }

    public Map<String, List<String>> getCrc() {
        return crc;
    }

    public void setCrc(Map<String, List<String>> crc) {
        this.crc = crc;
    }

    public byte getNameStatus() {
        return nameStatus;
    }

    public void setNameStatus(byte nameStatus) {
        this.nameStatus = nameStatus;
    }

    public byte getFileNameStatus() {
        return fileNameStatus;
    }

    public void setFileNameStatus(byte fileNameStatus) {
        this.fileNameStatus = fileNameStatus;
    }

    public byte getImageStatus() {
        return imageStatus;
    }

    public void setImageStatus(byte imageStatus) {
        this.imageStatus = imageStatus;
    }

    public byte getThumbnailStatus() {
        return thumbnailStatus;
    }

    public void setThumbnailStatus(byte thumbnailStatus) {
        this.thumbnailStatus = thumbnailStatus;
    }

    public byte getMarqueeStatus() {
        return marqueeStatus;
    }

    public void setMarqueeStatus(byte marqueeStatus) {
        this.marqueeStatus = marqueeStatus;
    }

    public byte getVideoStatus() {
        return videoStatus;
    }

    public void setVideoStatus(byte videoStatus) {
        this.videoStatus = videoStatus;
    }

    public byte getCrcStatus() {
        return crcStatus;
    }

    public void setCrcStatus(byte crcStatus) {
        this.crcStatus = crcStatus;
    }

    public Boolean isGame() {
        return game != null && game.getScreenscraper() != null ? game.getScreenscraper().getIsGame() : null;
    }

    public Boolean isClone() {
        return game != null
                && game.getScreenscraper() != null
                && game.getScreenscraper().getCloneOf() != null
                ? (game.getScreenscraper().getCloneOf().intValue() > 0) : null;
    }

    public String getRetrocenterID() {
        return game != null && game.getRetrocenterID() != null ? game.getRetrocenterID() : null;
    }

    public String getScreenscraperID() {
        return game != null && game.getScreenscraper() != null ? game.getScreenscraper().getId().toString() : null;
    }

    public Boolean isUnlicensed() {
        return game != null && game.getScreenscraper() != null ? game.getScreenscraper().getUnlicensed() : null;
    }

    public Boolean isAlternate() {
        return game != null && game.getScreenscraper() != null ? game.getScreenscraper().getAlternate() : null;
    }

    public Boolean isTranslated() {
        return game != null && game.getScreenscraper() != null ? game.getScreenscraper().getTranslated() : null;
    }

    public Boolean isDemo() {
        return game != null && game.getScreenscraper() != null ? game.getScreenscraper().getDemo() : null;
    }

    public Boolean isHack() {
        return game != null && game.getScreenscraper() != null ? game.getScreenscraper().getHack() : null;
    }

    public Boolean isProto() {
        return game != null && game.getScreenscraper() != null ? game.getScreenscraper().getProto() : null;
    }

    public Boolean isBeta() {
        return game != null && game.getScreenscraper() != null ? game.getScreenscraper().getBeta() : null;
    }

    @Override
    public int compareTo(AuditedGame o) {
        return game.compareTo(o.game);
    }

    @Override
    public String toString() {
        return "AuditedGame{" +
                "crc=" + crc +
                ", nameStatus=" + nameStatus +
                ", fileNameStatus=" + fileNameStatus +
                ", imageStatus=" + imageStatus +
                ", thumbnailStatus=" + thumbnailStatus +
                ", marqueeStatus=" + marqueeStatus +
                ", videoStatus=" + videoStatus +
                ", crcStatus=" + crcStatus +
                '}';
    }
}
