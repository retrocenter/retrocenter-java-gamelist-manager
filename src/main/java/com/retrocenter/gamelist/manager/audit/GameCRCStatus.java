package com.retrocenter.gamelist.manager.audit;

public final class GameCRCStatus {
    public static final byte OK = 1;
    public static final byte EMPTY = 2;
    public static final byte DUPLICATED = 4;
    public static final byte NOT_FOUND = 8;
    public static final byte MULTI = 16;
}
