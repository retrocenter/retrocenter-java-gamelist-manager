package com.retrocenter.gamelist.manager.audit;

import java.io.File;
import java.util.Collections;

import com.retrocenter.datafile.retrocenterdb.RetrocenterDB;
import com.retrocenter.datafile.retrocenterdb.RetrocenterGame;
import com.retrocenter.datafile.retrocenterdb.RetrocenterParser;

public class FindGamesWithoutName {
    public static void execute(File gamelist) {
        try {
            RetrocenterDB gl = RetrocenterParser.parse(gamelist);
            execute(gl);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void execute(RetrocenterDB gl) {
        Collections.sort(gl.getGames());
        for (int i = 1; i < gl.getGames().size(); i++) {
            RetrocenterGame game = gl.getGames().get(i);
            if (isEmptyName(game)) {
                System.out.println("@@@@@@@@@@ Sem Nome: " + game.getPath());
            }
        }
    }

    public static boolean isEmptyName(RetrocenterGame game) {
        return (game.getName() == null) || game.getName().trim().isEmpty();
    }
}
