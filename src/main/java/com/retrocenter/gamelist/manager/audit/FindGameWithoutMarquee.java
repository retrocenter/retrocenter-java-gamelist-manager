package com.retrocenter.gamelist.manager.audit;

import java.io.File;
import java.nio.file.Paths;

import com.retrocenter.datafile.retrocenterdb.RetrocenterGame;

public final class FindGameWithoutMarquee {
    public static boolean isMarqueeFilled(RetrocenterGame game) {
        if (game.getMarquee() == null || game.getMarquee().getPath().trim().isEmpty()) {
            return false;
        }
        return true;
    }

    public static boolean existMarqueeFile(String mediaBaseDir, String systemName, RetrocenterGame game) {
        String path = game.getMarquee().getPath();
        if (path.startsWith("./"))
            path = path.substring(2);
        File marquee = Paths.get(mediaBaseDir, systemName, path).toFile();
        return marquee.exists();
    }
}
