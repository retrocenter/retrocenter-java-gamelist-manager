package com.retrocenter.gamelist.manager.audit;

public final class GameImageStatus {
    public static final byte OK = 1;
    public static final byte EMPTY = 2;
    public static final byte NOT_FOUND = 4;
}
