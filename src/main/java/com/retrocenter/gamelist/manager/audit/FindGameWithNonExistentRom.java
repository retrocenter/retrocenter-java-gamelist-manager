package com.retrocenter.gamelist.manager.audit;

import java.io.File;
import java.nio.file.Paths;

import com.retrocenter.datafile.retrocenterdb.RetrocenterDB;
import com.retrocenter.datafile.retrocenterdb.RetrocenterGame;

public class FindGameWithNonExistentRom {
    public static void auditGameList(String romsBaseDir, String systemName, RetrocenterDB gameList) {
        for (RetrocenterGame game : gameList.getGames()) {
            auditGame(romsBaseDir, systemName, game);
        }
    }

    public static boolean auditGame(String romsBaseDir, String systemName, RetrocenterGame game) {
        boolean result = true;
        if (!isPathFilled(game)) {
            System.out.println("@@@@ PATH não configurado: " + game.getName());
            result = false;
        }
        if (!existRomFile(romsBaseDir, systemName, game)) {
            System.out.println("@@@@ ROM Inexistente: " + game.getPath());
            result = false;
        }
        return result;
    }

    public static boolean isPathFilled(RetrocenterGame game) {
        if (game.getPath() == null || game.getPath().trim().isEmpty()) {
            return false;
        }
        return true;
    }

    public static boolean existRomFile(String romsBaseDir, String systemName, RetrocenterGame game) {
        String path = game.getPath();
        if (path.startsWith("./"))
            path = path.substring(2);
        File rom = Paths.get(romsBaseDir, systemName, path).toFile();
        return rom.exists();
    }
}
