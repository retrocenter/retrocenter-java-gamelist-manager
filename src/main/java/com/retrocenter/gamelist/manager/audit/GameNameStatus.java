package com.retrocenter.gamelist.manager.audit;

public final class GameNameStatus {
    public static final byte OK = 1;
    public static final byte EMPTY = 2;
    public static final byte DUPLICATED = 4;
}
