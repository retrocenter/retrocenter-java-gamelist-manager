package com.retrocenter.gamelist.manager.audit;

import java.io.File;
import java.nio.file.Paths;

import com.retrocenter.datafile.retrocenterdb.RetrocenterGame;

public final class FindGameWithoutThumbnail {
    public static boolean isThumbnailFilled(RetrocenterGame game) {
        if (game.getThumbnail() == null || game.getThumbnail().getPath().trim().isEmpty()) {
            return false;
        }
        return true;
    }

    public static boolean existThumbnailFile(String mediaBaseDir, String systemName, RetrocenterGame game) {
        String path = game.getThumbnail().getPath();
        if (path.startsWith("./"))
            path = path.substring(2);
        File thumbnail = Paths.get(mediaBaseDir, systemName, path).toFile();
        return thumbnail.exists();
    }
}
