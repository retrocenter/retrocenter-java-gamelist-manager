package com.retrocenter.gamelist.manager;

import java.io.File;
import java.io.FileReader;
import java.util.Properties;

public class RetrocenterConfig {
    public static final String FRONTEND_BATOCERA = "batocera";

    private static final String CONFIG_FILENAME = "config.properties";

    private static final String SERVER_URL_PROP_NAME = "retrocenter.serverurl";
    private static final String BASE_DEST_DIR_PROP_NAME = "frontend.basedir";
    private static final String FRONTEND_NAME_PROP_NAME = "retrocenter.frontend";

    @Deprecated
    public static final String GAMELIST_DB_FULL_FILE = "gamelist_fulldb.xml";
    public static final String GAMELIST_DB_CLEAR_FILE = "gamelist.xml";
    public static final String RETROCENTER_DB_FILE_NAME = "retrocenter.xml";

    //    public static final String MEDIA_BASE_DIR = "G:\\RetroPie\\RetroPieMedia";
    //    public static final String ROMS_BASE_DIR = "G:\\RetroPie\\RetroPieRoms";
    //    public static final String GAMELIST_DB_FULL_FILE = "gamelist_fulldb.xml";
    //    public static final String GAMELIST_DB_CLEAR_FILE = "gamelist.xml";

    private static Properties config = null;

    public static synchronized void loadConfig() {
        if (config == null) {
            File props = new File(CONFIG_FILENAME);
            if (!props.exists()) {
                throw new RuntimeException(CONFIG_FILENAME + " file not found");
            }
            config = new Properties();
            try {
                config.load(new FileReader(props));

                if (config.getProperty(SERVER_URL_PROP_NAME) == null) {
                    throw new RuntimeException(SERVER_URL_PROP_NAME + " not configured");
                }
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public static String getSelectedFrontend() {
        loadConfig();
        String result = config.getProperty(FRONTEND_NAME_PROP_NAME);
        if (result == null) {
            throw new RuntimeException("Frontend não configurado");
        }
        return result;
    }

    public static String getServerURL() {
        loadConfig();
        return config.getProperty(SERVER_URL_PROP_NAME);
    }

    public static String getROMsBaseDestDir() {
        loadConfig();
        if (getSelectedFrontend().equals(FRONTEND_BATOCERA)) {
            return config.getProperty(BASE_DEST_DIR_PROP_NAME) + File.separator + "roms";
        }
        throw new UnsupportedOperationException();
    }

    public static String getMediaBaseDestDir(String platform) {
        if (getSelectedFrontend().equals(FRONTEND_BATOCERA)) {
            return getROMsBaseDestDir() + File.separator + platform + File.separator + "media";
        }
        throw new UnsupportedOperationException();
    }

    public static String getGamelistBaseDestDir(String platform) {
        if (getSelectedFrontend().equals(FRONTEND_BATOCERA)) {
            return getROMsBaseDestDir() + File.separator + platform;
        }
        throw new UnsupportedOperationException();
    }

    public static String getProperty(String name) {
        loadConfig();
        String value = config.getProperty(name);
        if (value == null) {
            throw new RuntimeException(name + " not configured");
        }
        return value;
    }

    public static String getProperty(String name, String defaultValue) {
        try {
            loadConfig();
        } catch (Exception e) {
            return defaultValue;
        }
        String value = config.getProperty(name);
        if (value == null) {
            value = defaultValue;
        }
        return value;
    }
}
